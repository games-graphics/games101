//
// Created by Göksu Güvendiren on 2019-05-14.
//

#include "Scene.hpp"
#include "Triangle.hpp"


void Scene::buildBVH() {
    printf(" - Generating BVH...\n\n");
    this->bvh = new BVHAccel(objects, 1, BVHAccel::SplitMethod::NAIVE);
}

Intersection Scene::intersect(const Ray& ray) const
{
    return this->bvh->Intersect(ray);
}

void Scene::sampleLight(Intersection& pos, float& pdf) const
{
    float emit_area_sum = 0;
    for (uint32_t k = 0; k < objects.size(); ++k) {
        if (objects[k]->hasEmit()) {
            emit_area_sum += objects[k]->getArea();
        }
    }
    float p = get_random_float() * emit_area_sum;
    emit_area_sum = 0;
    for (uint32_t k = 0; k < objects.size(); ++k) {
        if (objects[k]->hasEmit()) {
            emit_area_sum += objects[k]->getArea();
            if (p <= emit_area_sum) {
                objects[k]->Sample(pos, pdf);
                break;
            }
        }
    }
}

bool Scene::trace(
    const Ray& ray,
    const std::vector<Object*>& objects,
    float& tNear, uint32_t& index, Object** hitObject)
{
    *hitObject = nullptr;
    for (uint32_t k = 0; k < objects.size(); ++k) {
        float tNearK = kInfinity;
        uint32_t indexK;
        Vector2f uvK;
        if (objects[k]->intersect(ray, tNearK, indexK) && tNearK < tNear) {
            *hitObject = objects[k];
            tNear = tNearK;
            index = indexK;
        }
    }


    return (*hitObject != nullptr);
}



// Implementation of Path Tracing  一个光线和一个depth
Vector3f Scene::castRay(const Ray& ray, int depth) const
{
    Intersection intersection = Scene::intersect(ray);

    //光线和场景相交
    if (intersection.happened)
    {
        //入射光线的方向
        Vector3f wo = normalize(-ray.direction);
        //相加坐标点
        Vector3f p = intersection.coords;
        //法向量
        Vector3f N = normalize(intersection.normal);
        //对光源进行采样
        Intersection inter;
        float pdf_light = 0;
        sampleLight(inter, pdf_light);

        Vector3f x = inter.coords;
        Vector3f ws = normalize(x - p);
        Vector3f NN = normalize(inter.normal);

        Vector3f L_dir( 0, 0, 0 );
        // 从p点发出一条光线到x点，判断能否和光源相交
        intersect(Ray(p, ws));

        // 判断该点出发的p->x能否打到光源
        if ((intersect(Ray(p, ws)).coords - x).norm() < 0.01)
        {
            //计算p点的直接光照  eval 代表brdf wo表示光线进入，ws表示光线出去
            L_dir = inter.emit * intersection.m->eval(wo, ws, N) * dotProduct(ws, N) * dotProduct(-ws, NN) / ((x - p).norm() * (x - p).norm() * pdf_light);
        }
        // 间接光照
        Vector3f L_indir(0, 0, 0);
        float P_RR = get_random_float();
        if (P_RR < Scene::RussianRoulette)
        {
            //simple one direction
            Vector3f wi = intersection.m->sample(wo, N);

            L_indir = castRay(Ray(p, wi), depth) * intersection.m->eval(wo, wi, N) * dotProduct(wi, N) / intersection.m->pdf(wo, wi, N) / Scene::RussianRoulette;
        }
        return L_dir + L_indir;
    }
    Vector3f vec(0, 0, 0);
    return vec;
}